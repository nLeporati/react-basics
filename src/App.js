import React from "react";
import { render } from "react-dom";
// import Item from "./Item";
import SearchParams from "./SearchParams";
import { ANIMALS } from "@frontendmasters/pet";

const App = () => {
  return (
    <div>
      <h1>Despensa !</h1>
      {/* <Item name="Manzana" type="Fruta" quantity="2" />
      <Item name="Leche" type="Liquido" quantity="4" />
      <Item name="Huevos" type="General" quantity="12" /> */}
      <SearchParams />
    </div>
  );
};

render(<App />, document.getElementById("root"));
